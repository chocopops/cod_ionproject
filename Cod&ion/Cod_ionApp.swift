//
//  Cod_ionApp.swift
//  Cod&ion
//
//  Created by chocopops on 28/03/2021.
//

import SwiftUI

@main
struct Cod_ionApp: App {
    var body: some Scene {
        WindowGroup {
            ListDealView()
        }
    }
}
