//
//  SeachView.swift
//  Cod&ion
//
//  Created by chocopops on 31/03/2021.
//

import SwiftUI

struct SeachView: View {
    
    @State private var motsCle = ""
    @State private var distance = 50.0
    @State private var prix = 50.0
    @State private var categorie: Categories = Categories.highTech

    var body: some View {
//        VStack {
            Form{
                
                TextField("Mots Clé", text: $motsCle)
                    .padding()
                    .background(Color.gray)
                    .cornerRadius(15)
                
                GroupBox(label: Text("Distance max (Km)")){
                    Slider(value: $distance, in: 0...100)
                }
                
                GroupBox(label: Text("Prix maximum")) {
                    Slider(value: $prix, in: 0...1000)
                }
                
                GroupBox(label: Text("Catégorie")){
                    Picker("", selection: $categorie) {
                            ForEach(Categories.allCases, id: \.self) {
                                Text($0.rawValue)
                            }
                    }
                }
                
                
                HStack {
                    Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                        Image(systemName: "magnifyingglass")
                        Text("Recherche")
                    })
                    .padding()
                    
                }
            }
//        }
    }
}

struct SeachView_Previews: PreviewProvider {
    static var previews: some View {
        SeachView()
            .previewDevice("iPhone 12")
    }
}
