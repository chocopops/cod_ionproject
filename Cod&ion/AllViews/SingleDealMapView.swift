//
//  SingleDealMapView.swift
//  Cod&ion
//
//  Created by chocopops on 01/04/2021.
//

import SwiftUI
import MapKit

struct SingleDealMapView: View {
    
    @State var region: MKCoordinateRegion
    
    var body: some View {
        
        Map(coordinateRegion: $region,
            interactionModes: [.zoom],
            showsUserLocation: true,
            userTrackingMode:  .constant(.follow)
            //            annotationContent: CLLocationCoordinate2D(latitude: 43.612, longitude: 1.307)
        )
        .frame(width: 300, height: 200)
    }
    
}

struct SingleDealMapView_Previews: PreviewProvider {
    static var previews: some View {
        SingleDealMapView(region: MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 51.507222, longitude: -0.1275), span: MKCoordinateSpan(latitudeDelta: 0.5, longitudeDelta: 0.5))
        )
    }
}
