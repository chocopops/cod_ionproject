//
//  ListeBonPlans.swift
//  Cod&ion
//
//  Created by chocopops on 28/03/2021.
//

import SwiftUI

struct DealRow: View {
    var data: Deal
    
    var body: some View{
        HStack{
            Image("\(data.image)").resizable()
                .frame(width: 70, height: 50, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                .padding()
            
            VStack(alignment: .leading, spacing: 4){
                Text("\(data.title)").bold()
                Text("\(String(format: "%.2f", data.price))")
                Text("\(data.city)").lineLimit(3)
            }
        }
    }
}

struct ListDealView: View {
    
    var body: some View {
        NavigationView{
            List(dealGeneration()){ deal in
                NavigationLink(destination: DetailDealView(deal: deal)) {
                    DealRow(data: deal)
                }
            }
            .navigationBarTitle("Les Bon Plans")
            .navigationBarTitleDisplayMode(.inline)
        }
    }
}

//func rowColor(dealList: [Deal]) -> Color {
//    var isPair = dealList
//    if 4%2 {return Color.gray} else{return Color.green}
//}

struct ListeBonPlans_Previews: PreviewProvider {
    static var previews: some View {
        ListDealView()
            .previewDevice("iPhone 12")
    }
}
