//
//  ContentView.swift
//  Cod&ion
//
//  Created by chocopops on 28/03/2021.
//

import SwiftUI
// partie principale: "struct bon plan", nav inseré grasse au modificateur...
struct DetailDealView: View {
    
    let deal: Deal
    
    var body: some View {
        
        ScrollView {
            Image(deal.image)
                .resizable()
                .frame(width: 255, height: 200, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
            
            Divider()
            VStack(alignment: .leading, spacing: 16) {
                Text("Prix: \(String(format: "%.2f", deal.price)) Euros")
                
                Text("Description: ")
                
                Text(deal.description)
                    .font(.body)
                
            }
            
            Divider().accentColor(.red)
            SingleDealMapView(region: deal.localisation)
        }
        .navigationBarTitle(deal.title)
        .navigationBarTitleDisplayMode(.inline)
    }
}



struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        
        NavigationView {
            DetailDealView(deal: listDeals[0])
        }
        .previewDevice("iPhone 12")
    }
}
