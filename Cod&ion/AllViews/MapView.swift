//
//  MapView.swift
//  Cod&ion
//
//  Created by chocopops on 01/04/2021.
//

import SwiftUI
import MapKit

struct MapView: View {
    
    @State var region: MKCoordinateRegion
    
    var body: some View {
        Map(coordinateRegion: $region,
            interactionModes: [.zoom],
            showsUserLocation: true,
            userTrackingMode:  .constant(.follow)
//            annotationContent: CLLocationCoordinate2D(latitude: 43.612, longitude: -1.307)
        )
                .frame(width: 300, height: 200)
    }
    
}


struct MapView_Previews: PreviewProvider {
    static var previews: some View {
        MapView(region: MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 43.507222, longitude: -1.1275), span: MKCoordinateSpan(latitudeDelta: 0.5, longitudeDelta: 0.5))
)
            .previewDevice("iPhone 12")
    }
}

