//
//  LocalisationService.swift
//  Cod&ion
//
//  Created by chocopops on 01/04/2021.
//


import CoreLocation


class LocalisationService: NSObject, CLLocationManagerDelegate {
    
    let manager = CLLocationManager()
    var currentLocation: CLLocationCoordinate2D?
    
    override init() {
        super.init()
        manager.delegate = self
    }
    
    func start() {
        manager.requestWhenInUseAuthorization()
        manager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        currentLocation = locations.first?.coordinate
    }
   
}
