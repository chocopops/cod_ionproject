//
//  DealCreator.swift
//  Cod&ion
//
//  Created by chocopops on 02/04/2021.
//

import Foundation

class DealCreator {
    // instanciate location manager
    let  locationManager = LocalisationService()
    
    // deal parameters
    var title: String?
    var image: String?
    var price: Double?
    var discount: Double?
    var address: String?
    var zipCode: String?
    var city: String?
    var country: String?
    var description: String?
    var category: Categories? // enum
    
    var user: String? // creer user struct
    
  //  var localisation: MKCoordinateRegion
    var endValidity: Date?
    
    func setTitle(titre: String) -> Bool {
        if !titre.isEmpty{
            if titre.count > 4 {
                title = titre
                return true
            } else {
                print("title too short!")
                return false
            }
        } else {
            print("title is empty")
            return false
        }
    }
    
    func setImage(img: String) -> Bool {
        if !img.isEmpty {
            image = img
            return true
        } else {
            image = "maisonVerte"
            return false
        }
    }
}
