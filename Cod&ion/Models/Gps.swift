//
//  Gps.swift
//  Cod&ion
//
//  Created by chocopops on 29/03/2021.
//

import Foundation

struct Gps {
    private var latitude: Double
    private var longitude: Double
    
    mutating func setLatitude(latitude: Double) {
        self.latitude = latitude
    }
    
    mutating func setLongitude(longitude: Double) {
        self.longitude = longitude
    }
    
    func getLatitude() -> Double {
        return self.latitude
    }
    
    func getLongitude() -> Double {
        return self.longitude
    }
}
