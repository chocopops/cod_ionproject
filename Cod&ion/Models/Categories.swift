//
//  Categories.swift
//  Cod&ion
//
//  Created by chocopops on 31/03/2021.
//

import Foundation
 
enum Categories: String, CaseIterable{
    case vetements = "Vetements", multimedia = "Multimedia", mobilier = "Mobilier", highTech = "High- tech", voiture = "Voiture", coiffeur = "Coiffeur", locations = "Locations", coLocations = "Co- Locations", animaux = "Animaux", sport = "Sport", loisir = "Loisir", alimentaire = "Alimentaire"
}
