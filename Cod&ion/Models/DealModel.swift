//
//  BonPlanStruct.swift
//  Cod&ion
//
//  Created by chocopops on 29/03/2021.
//

import Foundation
import MapKit

struct Deal: Identifiable {
    var id = UUID()
    
    var title: String
    var image: String
    var price: Double
    var discount: Double?
    var address: String
    var zipCode: String
    var city: String
    var country: String
    var description: String
    var category: Categories // enum
    
    var user: String // creer user struct
    
    var localisation: MKCoordinateRegion
    var endValidity: Date?
    
}


var listDeals: [Deal] = [
    Deal(title: "Mon Premier Deal", image: "maisonVerte", price: 10.45,  address: "3 chemin des trucs", zipCode: "31000", city: "Toulouse", country: "France", description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor. Cras elementum ultrices diam. Maecenas ligula massa, varius a, semper congue, euismod non, mi. Proin porttitor, orci nec nonummy molestie, enim est eleifend mi, non fermentum diam nisl sit amet erat. Duis semper. Duis arcu massa, scelerisque vitae, consequat in, pretium a, enim. Pellentesque congue. Ut in risus volutpat libero pharetra tempor. Cras vestibulum bibendum augue. Praesent egestas leo in pede. Praesent blandit odio eu enim. Pellentesque sed dui ut augue blandit sodales. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aliquam nibh. Mauris ac mauris sed pede pellentesque fermentum. Maecenas adipiscing ante non diam sodales hendrerit.", category: Categories.sport, user: "surment Toi", localisation: MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 51.507222, longitude: -0.1275), span: MKCoordinateSpan(latitudeDelta: 0.5, longitudeDelta: 0.5))
    ),
    Deal(title: "Mon Premier Deal", image: "voiture", price: 10000,  address: "45 rue des choses", zipCode: "91000", city: "Evry", country: "France", description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor. Cras elementum ultrices diam. Maecenas ligula massa, varius a, semper congue, euismod non, mi. Proin porttitor, orci nec nonummy molestie, enim est eleifend mi, non fermentum diam nisl sit amet erat. Duis semper. Duis arcu massa, scelerisque vitae, consequat in, pretium a, enim. Pellentesque congue. Ut in risus volutpat libero pharetra tempor. Cras vestibulum bibendum augue. Praesent egestas leo in pede. Praesent blandit odio eu enim. Pellentesque sed dui ut augue blandit sodales. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aliquam nibh. Mauris ac mauris sed pede pellentesque fermentum. Maecenas adipiscing ante non diam sodales hendrerit.", category: Categories.sport, user: "surment Moi", localisation: MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 51.507222, longitude: -0.1275), span: MKCoordinateSpan(latitudeDelta: 0.5, longitudeDelta: 0.5))
    )
]


//func Test() -> String {
//    return("" + String(Int.random(in: 1...9)))
//}

func dealGeneration() -> [Deal] {
    for numero in 1...10 {
        listDeals.append(
            Deal(title: "Mon deal numero"+String(numero), image: "image", price:  Double(Int.random(in: 1...100)), address: "9 rue de la lune", zipCode: String(Int.random(in: 10...99))+"000", city: "Dans une ville", country: "Dans le monde", description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor. Cras elementum ultrices diam. Maecenas ligula massa, varius a, semper congue, euismod non, mi. Proin porttitor, orci nec nonummy molestie, enim est eleifend mi, non fermentum diam nisl sit amet erat. Duis semper. Duis arcu massa, scelerisque vitae, consequat in, pretium a, enim. Pellentesque congue. Ut in risus volutpat libero pharetra tempor. Cras vestibulum bibendum augue. Praesent egestas leo in pede. Praesent blandit odio eu enim. Pellentesque sed dui ut augue blandit sodales. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aliquam nibh. Mauris ac mauris sed pede pellentesque fermentum. Maecenas adipiscing ante non diam sodales hendrerit.", category: Categories.sport, user:"Utilisateur numero "+String(numero), localisation: MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 51.507222, longitude: -0.1275), span: MKCoordinateSpan(latitudeDelta: 0.5, longitudeDelta: 0.5))
            )
        )
    }
    return listDeals
}
